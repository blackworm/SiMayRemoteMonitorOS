﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SiMay.Core
{
    /// <summary>
    /// 应用关系关联常量
    /// </summary>
    public class ApplicationNameConstant
    {
        public const string REMOTE_SCREEN = "RemoteDesktopJob";

        public const string REMOTE_FILE = "FileManagerJob";

        public const string REMOTE_AUDIO = "RemoteAudioJob";

        public const string REMOTE_REGEDIT = "RemoteRegistryEditorJob";

        public const string REMOTE_SHELL = "RemoteShellJob";

        public const string REMOTE_STARTUP = "StartupManagerJob";

        public const string REMOTE_SYSMANAGER = "SystemManagerJob";

        public const string REMOTE_TCP = "TcpConnectionManagerJob";

        public const string REMOTE_VIDEO = "RemoteViedoJob";

        public const string REMOTE_FILE_TRANSPORT = "RemoteFileTransport";

        public const string REMOTE_DESKTOP = "RemoteDesktop";

        public const string REMOTE_KEYBOARD = "RemoteKeyboradJob";
    }
}
